package de.foezer.aopdemo.dao;

import org.springframework.stereotype.Component;

import de.foezer.aopdemo.Account;

@Component
public class AccountDAO {

	public void addAcount(Account theAccount, boolean vipFlag) {
		System.out.println(getClass().getSimpleName() + ": DOING MY DB WORK: ADDING AN ACCOUNT");
	}
	
	public boolean doWork() {
		System.out.println(getClass().getSimpleName() + ": doWork()");
		return false;
	}
}
