package de.foezer.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {
	
	@Pointcut("execution(* de.foezer.aopdemo.dao.*.*(..))")
	private void forDaoPackage() {}
	
	//@Before("execution(public void add*())")
	@Before("forDaoPackage()")
	public void beforeAddAcountAdvice() {
		System.out.println("\n=====>>> Executing @Brfore advice on addAcount()");
	}
	
	@Before("forDaoPackage()")
	public void performApiAnalytics() {
		System.out.println("\n=====>>> Performing API analytics");
	}
}
