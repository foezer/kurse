package de.foezer.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {

	public boolean addSillyMember() {
		System.out.println(getClass().getSimpleName() + ": DOING STUFF: ADDING AN MEMBERSHIP ACCOUNT");
		
		return true;
	}
	
	public void goToSleep() {
		System.out.println(getClass().getSimpleName() + ": I'm going to sleep now...");
	}
}
