package de.feozer.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import de.feozer.hibernate.demo.entity.Employee;

public class EmployeeDemo {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			// save object
			Employee employee = new Employee("Thomas", "Keck", "Exxcellent");
			
			session.beginTransaction();
			session.save(employee);
			System.out.println("Create: " + employee);
			session.getTransaction().commit();
			
			// get new session
			session = factory.getCurrentSession();
			
			// get object by primary key
			session.beginTransaction();
			Employee dbEployee = session.get(Employee.class, employee.getId());
			System.out.println("Read: " + dbEployee);
			session.getTransaction().commit();
			
			// get new session
			session = factory.getCurrentSession();
			
			// get all objects by company
			session.beginTransaction();
			List<Employee> list = session.createQuery("from Employee e where e.company='exxcellent'").getResultList();
			for (Employee entry : list) {
				System.out.println(entry);
			}
			session.getTransaction().commit();
			
			
			// get new session
			session = factory.getCurrentSession();
			
			// delete object
			session.beginTransaction();
			session.delete(dbEployee);
			System.out.println("delete: " + dbEployee);
			session.getTransaction().commit();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			factory.close();
		}

	}

}
