package de.foezer.springboot.thymeleafdemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import de.foezer.springboot.thymeleafdemo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
