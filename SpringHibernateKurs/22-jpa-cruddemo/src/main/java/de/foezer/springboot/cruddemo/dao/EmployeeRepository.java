package de.foezer.springboot.cruddemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import de.foezer.springboot.cruddemo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
