package de.foezer.springdemo;

public class FitnessCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public FitnessCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Practice push ups every morning";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
