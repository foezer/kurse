package de.foezer.springdemo;

import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;



@Component
public class RandomFortuneServiceFromFile implements FortuneService {

	// create an array of strings
	private List<String> data = new ArrayList<>();
	
	// create a random number generator
	private Random myRandom = new Random();
	
	@PostConstruct
	public void readFortunesFromFile() {
		
		System.out.println(">> RandomFortuneServiceFromFile: inside of readFortunesFromFile()");

		try (FileReader fr = new FileReader("C:\\Users\\faruk\\Documents\\Kurse\\SpringKursEclipseTeil2\\src\\fortune.txt");
				BufferedReader br = new BufferedReader(fr);) {
			while(br.ready()) {
				data.add(br.readLine());
			}
		} catch(IOException e) {
			System.err.println(e);
		}
	}
	
	@Override
	public String getFortune() {
		// pick a random String from the array
		int index = myRandom.nextInt(data.size());
		
		String theFortune = data.get(index);
		
		return theFortune;
	}

}
