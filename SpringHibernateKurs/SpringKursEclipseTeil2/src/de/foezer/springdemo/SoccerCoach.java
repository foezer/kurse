package de.foezer.springdemo;

import org.springframework.stereotype.Component;

@Component
public class SoccerCoach implements Coach {

	@Override
	public String getDailyWorkout() {
		return "Practice your skills at standard situations";
	}

	@Override
	public String getDailyFortune() {
		return null;
	}

}
