package de.foezer.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PracticeSevenJavaConfig {

	@Bean
	public MyLoggerConfig logger() {
		return new MyLoggerConfig();
	}
	
	@Bean
	public FortuneService staticFortuneService() {
		return new StaticFortuneService();
	}
	
	@Bean
	public Coach fitnessCoach() {
		return new FitnessCoach(staticFortuneService());
	}
	
}
