package de.foezer.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifeCycleDemoApp {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle-ApplicationContext.xml");
		
		Coach theCouch = context.getBean("myCoach", Coach.class);
					
		System.out.println(theCouch.getDailyWorkout());
		
		context.close();
	}

}
