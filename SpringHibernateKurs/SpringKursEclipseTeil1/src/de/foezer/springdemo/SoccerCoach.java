package de.foezer.springdemo;

public class SoccerCoach implements Coach {

	//define a private field for the dependency
	private FortuneService fortuneService;
	
	// define a constructor for dependency injection
	public SoccerCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Train standard situation: Corners, Freekicks";
	}

	@Override
	public String getDailyFortune() {
		// use my fortuneService to get a fortune
		return fortuneService.getFortune();
	}

}
