package de.foezer.springdemo;

public class RandomFortuneService implements FortuneService {
	
	private String[] fortune = {"Lucky","Normal","Unlucky"};

	@Override
	public String getFortune() {
		return fortune[(int) (Math.random() * 10 % 3)];
	}

}
