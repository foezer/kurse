package de.foezer.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-ApplicationContext.xml");
		
		Coach theCouch = context.getBean("myCoach", Coach.class);
				
		Coach alphaCouch = context.getBean("myCoach", Coach.class);
		
		boolean result = (theCouch == alphaCouch);
		
		System.out.println("\nPointing to the same object: " + result);
		
		System.out.println("\nMemory location for theCoach: " + theCouch);
		System.out.println("\nMemory location for theCoach: " + alphaCouch + "\n");
		
		context.close();
	}

}
