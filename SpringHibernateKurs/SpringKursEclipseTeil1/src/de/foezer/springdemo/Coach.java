package de.foezer.springdemo;

public interface Coach {
	public String getDailyWorkout();
	public String getDailyFortune();
}
